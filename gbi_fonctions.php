<?php


/**
 * Produit un CSS ou JS statique
 *
 * Si un fichier existe, l'utilise,
 * Sinon si un squelette existe, l'utilise.
 *
 * @example
 *     `gbi_chemin_statique('css/expo.css');`
 *     `gbi_chemin_statique('css/expo.css', 'css');`
 *
 *     Ce code cherche dans le path
 *     - 'css/expo.css' et l'utilise s'il le trouve,
 *     - Sinon (uniquement avec CSS) 'css/expo.less', le calcule et l'utilise (si le plugin LESS-CSS est présent)
 *     - Sinon 'css/expo.css.html', le calcule et l'utilise
 * 
 * @param string $chemin
 *     Chemin (dans le path SPIP). Exemple 'css/perso.css'
 * @param string $extension
 *     Type de fichier attendu ('css' ou 'js')
 *     Si vide, sera calculé en utilisant l'extension de $chemin
 * @return string
 *     - URL du fichier CSS ou JS
 *     - vide si pas de fichier trouvé
**/
function gbi_chemin_statique($chemin, $extension = '') {
	static $done = false;

	if (!$extension) {
		if (preg_match(',[.](css|js|json)$,', $chemin, $m)) {
			$extension = $m[1];
		}
	}

	if (!$done and $extension == 'css') {
		$done = true;
		include_spip('lesscss_fonctions');
	}

	if (!$f = find_in_path($chemin)) {
		if ($extension == 'css' and function_exists('lesscss_select_css')) {
			$f = lesscss_select_css($chemin);
		} else {
			$f = trouver_fond($chemin)
				? produire_fond_statique($chemin, array('format' => $extension))
				: '';
		}
	}
	return $f ? timestamp(direction_css($f)) : '';
}


/**
 * Enlève le texte 'Source' sur un texte
 *
 * @param string $c
 *     Le texte d'origine
 * @return string
 *     Texte sans 'Source'
**/
function gbi_cacher_source($c){
	if ($num=strpos($c,"<br />Source")){
		return substr($c,0,$num);
	}
	return $c;
}



function balise_URL_CREDITS_dist($p) {
	return  calculer_balise_url_gbi($p, 'credits');
}

/* calcul l'url demandee la plus proche de la rubrique donnee */
function balise_URL_EXPOSITION_dist($p) {
	return  calculer_balise_url_parent_gbi($p, 'exposition');
}

function balise_URL_BORNE_dist($p) {
	return  calculer_balise_url_parent_gbi($p, 'borne');
}

function balise_URL_PLANCHE_dist($p) {
	return  calculer_balise_url_parent_gbi($p, 'planche');
}

/* calcul l'id demandee la plus proche de la rubrique donnee */
function balise_ID_EXPOSITION_dist($p) {
	return  calculer_balise_id_parent_gbi($p, 'exposition');
}

function balise_ID_BORNE_dist($p) {
	return  calculer_balise_id_parent_gbi($p, 'borne');
}

function balise_ID_PLANCHE_dist($p) {
	return  calculer_balise_id_parent_gbi($p, 'planche');
}


/* retourne une url de type nomXX, ou XX est un id_rubrique */
function calculer_balise_url_gbi($p, $nom){
	$_id = interprete_argument_balise(1,$p);
	if (!$_id) $_id = champ_sql('id_rubrique', $p);
	$p->code = "vider_url(generer_url_entite($_id, $nom))";
	$p->interdire_scripts = false;
	return $p;
}

/* calcule et retourne une url de type rubriqueXX, ou XX est un id_rubrique
 * appartenant a un type de rubrique demande
 * types : 'planche', 'borne', 'exposition'
 * avec cet ordre logique : planche appartient à borne, qui appartient a une exposition
 * */
function calculer_balise_url_parent_gbi($p, $nom){
	$_id = interprete_argument_balise(1,$p);
	if (!$_id) $_id = champ_sql('id_rubrique', $p);
	$p->code = "gbi_generer_url_parent($_id, $nom)";
	$p->interdire_scripts = false;
	return $p;
}

/* retourne une id de type id_rubrique correspondant a la demande */
function calculer_balise_id_parent_gbi($p, $nom){
	$_id = interprete_argument_balise(1,$p);
	if (!$_id) $_id = champ_sql('id_rubrique', $p);
	$p->code = "gbi_generer_id_parent($_id, $nom)";
	$p->interdire_scripts = false;
	return $p;
}

function gbi_generer_id_parent($id_rubrique, $type) {
	return gbi_calculer_parent($id_rubrique, $type);
}

function gbi_generer_url_parent($id_rubrique, $type) {
	if ($id = gbi_calculer_parent($id_rubrique, $type)) {
		return generer_url_entite($id, 'rubrique');
	}
	return '';
}

function gbi_calculer_parent($id_rubrique, $type) {

	$possibles = array('planche', 'borne', 'exposition');
	
	$res = sql_fetsel(array('composition', 'id_parent'), 'spip_rubriques','id_rubrique='.sql_quote($id_rubrique));
	if (!$res) return '';
	if ($compo = $res['composition']) {
		if (strncmp($compo, $type.'_', strlen($type)+1) === 0) {
			return $id_rubrique;
		}
	}
	
	$id_parent = $res['id_parent'];
	while ($res = sql_fetsel(array('composition', 'id_parent'), 'spip_rubriques', 'id_rubrique='.sql_quote($id_parent))) {
		if ($compo = $res['composition']) {
			if (strncmp($compo, $type.'_', strlen($type)+1) === 0) {
				return $id_parent;
			}	
		}
		$id_parent = $res['id_parent'];
	}
	
	return '';
}


/**
 * Extraire la clé d'expo d'une composition
 *
 * Extrait 'qqc' dans ces expressions :
 * - '{type}_{qqc}'
 * - '{type}_{qqc}_{autre_chose}'
 * - type peut être 'exposition', 'borne' ou 'planche'
 *
 * Exemples : extrait 'dragons' dans :
 * - exposition_dragons
 * - planche_dragons_catalogue

 * @param string $composition
 *     Nom de la composition
 * @return string
 *     Nom de l'expo ('qqc'), sinon vide
 */
function gbi_extraire_expo($composition) {
	$possibles = array('planche', 'borne', 'exposition');
	foreach ($possibles as $t) {
		if (strncmp($composition, $t.'_', strlen($t)+1) === 0) {
			$suite = substr($composition, strlen($t)+1);
			$suite = explode('_', $suite);
			return array_shift($suite);
		}
	}
	return '';
}

?>
