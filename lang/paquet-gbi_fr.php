<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-gbi
// Langue: fr
// Date: 18-07-2013 16:25:26
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'gbi_description' => 'Gestion de bornes intéractives d\'exposition.
	Ce plugin sert de base pour créer des schémas de bornes.',
	'gbi_slogan' => 'Gestion de bornes intéractives d\'exposition',
);
?>