<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Déclarer les champs extras 
**/
function gbi_declarer_champs_extras($champs = array()){

	$champs['spip_rubriques']['variante_borne'] = array(
		'saisie' => 'input',//Type du champs (voir plugin Saisies)
		'options' => array(
			'nom' => 'variante_borne', 
			'label' => _T('gbi:label_variante_borne'), 
			'sql' => "varchar(30) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'explication' => _T('gbi:explication_variante_borne'),
		),
		'verifier' => array());

	$champs['spip_articles']['sources'] = array(
		'saisie' => 'input',//Type du champs (voir plugin Saisies)
		'options' => array(
			'nom' => 'sources', 
			'label' => _T('gbi:label_sources'), 
			'sql' => "text NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
		),
		'verifier' => array());

	$champs['spip_articles']['pos_x'] = array(
		'saisie' => 'input',//Type du champs (voir plugin Saisies)
		'options' => array(
			'nom' => 'pos_x', 
			'label' => _T('gbi:label_pos_x'), 
			'sql' => "double NOT NULL DEFAULT 0",
			'defaut' => '',// Valeur par défaut
		),
		'verifier' => array());

	$champs['spip_articles']['pos_y'] = array(
		'saisie' => 'input',//Type du champs (voir plugin Saisies)
		'options' => array(
			'nom' => 'pos_y', 
			'label' => _T('gbi:label_pos_y'), 
			'sql' => "double NOT NULL DEFAULT 0",
			'defaut' => '',// Valeur par défaut
		),
		'verifier' => array());

	$champs['spip_articles']['data'] = array(
		'saisie' => 'textarea',//Type du champs (voir plugin Saisies)
		'options' => array(
			'nom' => 'data', 
			'label' => _T('gbi:label_data'), 
			'sql' => "text NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
		),
		'verifier' => array());

	return $champs;
}

?>
