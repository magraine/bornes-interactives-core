<?php


/**
 * Insérer les CSS et JS des bornes d'expos
 *
 * @param array $flux
 *     - array 'args'  : description pour le pipeline
 *       - clé 'expo'  : nom-type de l'expo
 *     - string 'data' : retour (texte)
 * @return array
**/
function gbi_insert_head_gbi($flux){

	// js - css globaux
	$add = '<link rel="stylesheet" href="' . _DIR_PLUGIN_GBI . 'gbi.css' . '" type="text/css" media="all" />'
		 . '<script type="text/javascript" src="' . _DIR_PLUGIN_GBI . 'javascript/gbi.js' . '" ></script>';

	if (isset($flux['args']['expo'])
	  and $expo = $flux['args']['expo'])
	{
		include_spip('gbi_fonctions');

		// js spécifiques à l'expo
		if ($js = gbi_chemin_statique("gbi_$expo.js")) {
			$add .= "<script type='text/javascript' src='$js'></script>";
		}

		// css spécifiques à l'expo
		if ($css = gbi_chemin_statique("gbi_$expo.css")) {
			$add .= "<link rel='stylesheet' href='$css' type='text/css' media='all' />";
		}
	}

	$flux['data'] .= $add;

	return $flux;
}


/**
 * Gérer les url de crédits
 *
 * @param array $array Liste des objets
 * @return array
**/ 
function gbi_declarer_url_objets($array) {
    $array[] = 'credits';
    return $array;
}

?>
